# README #

### What is this repository for? ###

A simple list field-type for Pyrocms 3 that allows you to add dynamic lists to items without end users breaking the layout.

Reorder, Add more lines, remove them
### How do I get set up? ###

1 Download the repo

2 Place it in /addons/site-ref/Emange/

### How do I use this? ###

Returns an array of values
{{ emange.field_type.list.values }}

//Returns the number of list items
{{ emange.field_type.list.count }}

### Demo? ###
![ft%20demo[1].gif](https://bitbucket.org/repo/gaXKzn/images/807605579-ft%2520demo%5B1%5D.gif)
http://edi.mange.biz/files/module_items/ft%20demo.gif

### Who do I talk to? ###

Reach out to me on the PyroCms slack, my handle is edster