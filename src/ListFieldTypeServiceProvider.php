<?php namespace Emange\ListFieldType;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

class ListFieldTypeServiceProvider extends AddonServiceProvider
{
    protected $singletons = [
        'Emange\ListFieldType\ListFieldTypeModifier' => 'Emange\ListFieldType\ListFieldTypeModifier'
    ];
}
