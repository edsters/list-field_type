<?php namespace Emange\ListFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypePresenter;

/**
 * Class ListFieldTypePresenter
 *
 * @link          http://edi.mange.biz/
 * @author        Emange. <edi@mange.biz>
 * @author        Edi Mange <edi@mange.biz>
 * @package       Emange\ListFieldType
 */
class ListFieldTypePresenter extends FieldTypePresenter
{

    /**
     * The decorated object.
     * This is for IDE hinting.
     *
     * @var ListFieldType
     */
    protected $object;

    /**
     * Return the number of selections.
     *
     * @return int
     */
    public function count()
    {
        return count($this->values());
    }

    /**
     * Return the option values.
     *
     * @return array
     */
    public function values()
    {
        return array_values($this->selections());
    }

    public function selections()
    {
        return $this->object->getValue();
    }

}
