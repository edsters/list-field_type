<?php namespace Emange\ListFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypeModifier;

/**
 * Class ListFieldType
 *
 * @link          http://edi.mange.biz/
 * @author        Emange. <edi@mange.biz>
 * @author        Edi Mange <edi@mange.biz>
 * @package       Emange\FontawesomeFieldType
 */
class ListFieldTypeModifier extends FieldTypeModifier
{

    /**
     * Return the serialized value.
     *
     * @param $value
     * @return string
     */
    public function modify($value)
    {
        if (is_string($value)){
            return $value;
        };
        
        return serialize(array_filter((array)$value));
    }

    /**
     * Restore the value.
     *
     * @param $value
     * @return mixed
     */
    public function restore($value)
    {
        if (!$value) {
            return [];
        }

        if (is_array($value)) {
            return array_filter($value);
        }

        return array_filter((array)unserialize($value));
    }
}
