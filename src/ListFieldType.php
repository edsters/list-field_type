<?php namespace Emange\ListFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldType;

/**
 * Class FontawesomeFieldType
 *
 * @link          http://edi.mange.biz/
 * @author        Emange. <edi@mange.biz>
 * @author        Edi Mange <edi@mange.biz>
 * @package       Emange\FontawesomeFieldType
 */
class ListFieldType extends FieldType
{

    /**
     * The database column type.
     *
     * @var string
     */
    protected $columnType = 'text';

    /**
     * The field input view.
     *
     * @var string
     */
    protected $inputView = 'emange.field_type.list::input';


    /**
     * The config array.
     *
     * @var array
     */
    protected $config = [
        'type' => 'text'
    ];

    /**
     * Get the rules.
     *
     * @return array
     */
    public function getRules()
    {
        $rules = parent::getRules();

        if ($min = $this->config('min')) {
            $rules[] = 'min:' . $min;
        }

        if ($max = $this->config('max')) {
            $rules[] = 'max:' . $max;
        }

        return $rules;
    }
}
