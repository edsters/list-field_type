# List Field Type

- [General Information](#general)

<hr>

<a name="general"></a>
## General Information

`emange.field_type.list.`

The list field type provides a list of items making it so the user cannot break formatting on pages, but can add a list of variable sizes.