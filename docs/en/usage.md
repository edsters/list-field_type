# Usage

- [Setting Values](#mutator)
- [Basic Output](#output)
- [Presenter Output](#presenter)

<hr>

<a name="mutator"></a>
## Setting Values

You must set the list field type value with a value or values from the available options.

    $entry->example = "foo";

You can set multiple values with an array.

    $entry->example = ["foo", "bar"];

<hr>

<a name="output"></a>
## Basic Output

The list field type returns an array of values.

    $entry->example->values; // ["foo", "bar"];