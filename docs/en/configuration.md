# Configuration

- [Basic Configuration](#basic)

<hr>

Below is the full configuration available with defaults.

    protected $fields = [
        "example" => [
            "type"   => "emange.field_type.list",
            "config" => [
                "min" => null,
                "max" => null
            ]
        ]
    ];

<hr>

<a name="basic"></a>
## Basic Configuration

### Default Value

    "default_type" => "text"

The `default_value` allows you to set the input type for validation. Supports text and email.

<hr>